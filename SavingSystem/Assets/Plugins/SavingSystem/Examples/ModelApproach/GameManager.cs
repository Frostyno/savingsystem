using ManicGames.SavingSystem;
using UnityEngine;

namespace ManicGames
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        string _saveName = "save.dat";
        
        private void Start()
        {
            SaveManager.Instance.Load(_saveName);
        }

        private void OnDestroy()
        {
            SaveManager.Instance.Save(_saveName);
        }
    }
}
