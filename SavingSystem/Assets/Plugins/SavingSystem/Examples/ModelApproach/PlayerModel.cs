using ManicGames.SavingSystem;
using ManicGames.SavingSystem.Examples;
using UnityEngine;

namespace ManicGames
{
    public class PlayerModel : SavableBehaviour<PlayerState>
    {
        [SerializeField]
        PlayerController _player;
        
        public override object GetState()
        {
            return new PlayerState
            {
                Position = transform.position,
                WasInvisible = _player.WasInvisible
            };
        }

        protected override void LoadState(PlayerState state)
        {   
            transform.position = state.Position;
            _player.WasInvisible = state.WasInvisible;
        }
    }

    [System.Serializable]
    public class PlayerState
    {
        public Vector3S Position;
        public bool WasInvisible;
    }
}
