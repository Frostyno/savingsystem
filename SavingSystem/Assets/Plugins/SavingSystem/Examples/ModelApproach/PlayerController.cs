using UnityEngine;

namespace ManicGames.SavingSystem.Examples
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        float _movementSpeed;
        
        // Bad practice, only like this because stupid bolt
        public bool WasInvisible { get; set; }

        private void Update()
        {
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(_movementSpeed * Time.deltaTime * Vector2.right);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(_movementSpeed * Time.deltaTime * Vector2.left);
            }
        }

        private void OnBecameInvisible()
        {
            WasInvisible = true;
        }
    }
}
