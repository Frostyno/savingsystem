namespace ManicGames.SavingSystem
{
    public interface ISavable
    {
        string Key { get; }
        object GetState();
        void LoadState(object state);
    }
}