using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace ManicGames.SavingSystem
{
	#if UNITY_EDITOR
	public static class KeyGenerator
	{
		static string _guidFilePath = Path.Combine(Application.dataPath, "Plugins", "SavingSystem");
		static string _guidFileName = "guids.dat";

		static HashSet<string> _assignedGuids;

		public static string GetGuid()
		{
			EnsureGuidsCacheExists();
			var guid = string.Empty;
			do
			{
				guid = System.Guid.NewGuid().ToString();
			} 
			while (_assignedGuids.Contains(guid));

			_assignedGuids.Add(guid);
			FileHandler.Write(_assignedGuids, _guidFilePath, _guidFileName);
			return guid;
		}

		public static void ReleaseGuid(string guid)
		{
			EnsureGuidsCacheExists();
			_assignedGuids?.Remove(guid);
			FileHandler.Write(_assignedGuids, _guidFilePath, _guidFileName);
		}

		static void EnsureGuidsCacheExists()
		{
			if (_assignedGuids == null)
			{
				_assignedGuids = FileHandler.Read<HashSet<string>>(_guidFilePath, _guidFileName);
				_assignedGuids = _assignedGuids ?? new HashSet<string>();
			}
		}
	}
	#endif
}
