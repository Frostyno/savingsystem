using UnityEngine;

namespace ManicGames.SavingSystem
{
    [System.Serializable]
    public struct Vector2S
    {
        private float _x;
        private float _y;

        public Vector2S(float x, float y)
        {
            _x = x;
            _y = y;
        }

        public static implicit operator Vector2(Vector2S v)
        {
            return new Vector2(v._x, v._y);
        }

        public static implicit operator Vector2S(Vector2 v)
        {
            return new Vector2S(v.x, v.y);
        }
    }

    [System.Serializable]
    public struct Vector3S
    {
        private float _x;
        private float _y;
        private float _z;

        public Vector3S(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        public static implicit operator Vector3(Vector3S v)
        {
            return new Vector3(v._x, v._y, v._z);
        }

        public static implicit operator Vector3S(Vector3 v)
        {
            return new Vector3S(v.x, v.y, v.z);
        }
    }
}
