using System.Collections.Generic;
using UnityEngine;
using SaveCollection = System.Collections.Generic.Dictionary<string, object>;

namespace ManicGames.SavingSystem
{
	public class SaveManager
	{
		static SaveManager _instance;
		public static SaveManager Instance => _instance ?? (_instance = new SaveManager());

		Dictionary<string, ISavable> _savables = new Dictionary<string, ISavable>();
		SaveCollection _cache = new SaveCollection();

		public void Register(ISavable savable)
		{
			if (savable == null)
			{
				return;
			}

			_savables[savable.Key] = savable;
		}

		public void Unregister(ISavable savable, bool cacheState = true)
		{
			if (savable == null)
			{
				return;
			}

			if (cacheState)
			{
				AddStateToCollection(savable, _cache);
			}
			
			_savables.Remove(savable.Key);
		}

		/// <summary>
		/// Saves current game state into a file. Note that this state is a combination of cached and
		/// currently registered savables (and their states). If retainSavedValues is true, will load
		/// values from existing save file and retain other saved values.
		/// </summary>
		/// <param name="fileName">Name of file that the state should be saved to.</param>
		/// <param name="retainSavedValues">Should old save file be loaded or overwritten?</param>
		public void Save(string fileName, bool retainSavedValues = true)
		{
			if (string.IsNullOrEmpty(fileName))
			{
				return;
			}

			var state = retainSavedValues ? ReadSaveFile(fileName) : new SaveCollection(_savables.Count);
			foreach (var savable in _savables.Values)
			{
				AddStateToCollection(savable, state);
			}

			if (_cache.Count > 0)
			{
				state.SetRange(_cache);
				_cache.Clear();
			}
			
			FileHandler.Write(state, Application.persistentDataPath, fileName);
		}

		public void Load(string fileName)
		{
			var state = ReadSaveFile(fileName);
			foreach (var data in state)
			{
				if (!_savables.TryGetValue(data.Key, out var savable))
				{
					continue;
				}
				
				savable.LoadState(data.Value);
			}
		}

		private void AddStateToCollection(ISavable savable, SaveCollection collection)
		{
			var state = savable.GetState();
			if (!state.GetType().IsSerializable)
			{
				Debug.LogWarning($"State needs to be Serializable in order to be saved! Type: {state.GetType().Name}");
				return;
			}

			collection[savable.Key] = state;
		}

		private SaveCollection ReadSaveFile(string fileName)
		{
			var state = FileHandler.Read<SaveCollection>(Application.persistentDataPath, fileName);
			return state ?? new SaveCollection();
		}
	}
}
