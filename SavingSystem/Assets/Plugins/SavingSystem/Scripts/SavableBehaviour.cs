using System;
using UnityEngine;

namespace ManicGames.SavingSystem
{
    public abstract class SavableBehaviour<T> : MonoBehaviour, ISavable where T : class
    {
        [SerializeField]
        string _savableGuid;
        
        public string Key => _savableGuid;

        protected virtual void Awake()
        {
            SaveManager.Instance.Register(this);
        }

        private void OnDestroy()
        {
            SaveManager.Instance.Unregister(this);
        }

        #if UNITY_EDITOR
        private void OnValidate()
        {
            if (string.IsNullOrEmpty(_savableGuid))
            {
                this.SerializeString(nameof(_savableGuid), KeyGenerator.GetGuid());
            }
        }
        #endif

        public void LoadState(object state)
        {
            if (!(state is T s))
            {
                Debug.LogError($"Failed to cast saved state of type {state.GetType().Name}. Expected type: {typeof(T).Name}");
                return;
            }
            
            LoadState(s);
        }

        public abstract object GetState();
        protected abstract void LoadState(T state);
    }
}