using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace ManicGames.SavingSystem
{
    public static class Extensions
    {
        public static void SerializeString(this Component component, string propertyName, string value)
        {
            var serializedObject = new SerializedObject(component);
            var property = serializedObject.FindProperty(propertyName);
            if (property == null)
            {
                return;
            }

            property.stringValue = value;
            serializedObject.ApplyModifiedProperties();
        }

        public static void SetRange<TKey, TValue>(this Dictionary<TKey, TValue> dict, Dictionary<TKey, TValue> other)
        {
            foreach (var pair in other)
            {
                dict[pair.Key] = pair.Value;
            }
        }
    }
}