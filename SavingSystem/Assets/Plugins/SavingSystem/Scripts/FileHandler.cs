using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace ManicGames.SavingSystem
{
    public static class FileHandler
    {
        public static void Write(object data, string path, string fileName)
        {
            if (!CheckWriteConditions(data, path))
            {
                return;
            }

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var fullPath = Path.Combine(path, fileName);
            using (var file = File.Open(fullPath, FileMode.Create))
            {
                var writer = new BinaryFormatter();
                writer.Serialize(file, data);
                file.Close();
            }
        }

        public static T Read<T>(string path, string fileName)
        {
            var fullPath = Path.Combine(path, fileName);
            if (!File.Exists(fullPath))
            {
                return default;
            }

            T contents = default;
            using (var file = File.Open(fullPath, FileMode.Open))
            {
                var reader = new BinaryFormatter();
                try
                {
                    contents = (T) reader.Deserialize(file);
                }
                catch (Exception e)
                {
                    Debug.LogError($"Failed to read save file on path '{fullPath}'. Reason: {e.Message}");
                }
            }

            return contents;
        }

        static bool CheckWriteConditions(object data, string path)
        {
            if (!data.GetType().IsSerializable)
            {
                Debug.LogError($"Object of type '{data.GetType().Name}' is not serializable and won't be saved.");
                return false;
            }

            if (string.IsNullOrEmpty(path))
            {
                Debug.LogError("Path needs to be specified in order to save to file.");
                return false;
            }

            return true;
        }
    }
}